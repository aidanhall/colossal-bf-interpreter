# Colossal BF Interpreter
The name is a reference to my previous attempt to write a BF
interpreter in Rust (`tiny-bf-interpreter`) which failed.
This program is fairly colossal for what it does, but it is
technically a compiler, or JIT interpreter, since the whole program
must be read and parsed before it can be executed.

A REPL variant could be easily created by reading a line at a time, or
until a balanced set of brackets has been input, and executing that
block of code with the existing memory and read/write pointer
position.

This is fairly standard BF, with the addition of the `@` command to
output the numerical value at the memory pointer.

Error messages are displayed using Rust's debug structure printing
format (`"{:?}"`).

## Usage
Running a program from the first command-line argument:

``` shell
$ cargo run ",>,<[->+<]>@"
```

Running a program read from standard input:

``` shell
$ cat hello.bf | cargo run -- -i
```

``` shell
$ cargo run -- -i
,> Input in first cell.
++ 2 in second cell
[<.>-] Output input twice.
```

