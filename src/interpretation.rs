use console::Term;
use std::io::Write;
use std::io;

use crate::parsing::Command;

pub fn execute(prog: Vec<Command>) {
    let term = Term::stdout();
    println!("Executing BF Program:\n{:?}", prog);

    const MEMORY_LENGTH: usize = 30000;
    let mut memory: [u8; MEMORY_LENGTH] = [0; MEMORY_LENGTH];

    // Data pointer.
    let mut ptr: usize = 0;
    // Instruction pointer.
    let mut ins_ptr: usize = 0;

    while ins_ptr < prog.len() {
        let mut jumped: bool = false;
        match prog[ins_ptr] {
            Command::Inc => memory[ptr] += 1,
            Command::Dec => memory[ptr] -= 1,
            Command::Left => ptr -= 1,
            Command::Right => ptr += 1,
            Command::Output => print!("{}", memory[ptr]),
            Command::OutputAscii => print!("{}", memory[ptr] as char),
            Command::Input => {
                memory[ptr] = {
                    match term.read_char() {
                        Ok(c) => c as u8,
                        Err(e) => {
                            eprintln!("Couldn't read input: {}", e);
                            memory[ptr]
                        }
                    }
                }
            }

            Command::JumpForwardIfZero { addr } => {
                if memory[ptr] == 0 {
                    ins_ptr = addr;
                    jumped = true;
                }
            }
            Command::JumpBack { addr } => {
                ins_ptr = addr;
                jumped = true;
            }
            _ => panic!("Undefined command!"),
        }

        if !jumped {
            ins_ptr += 1;
        }
    }
    // Flush output at the end.
    io::stdout().flush().expect("fflush()? really?!");
}
