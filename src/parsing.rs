#[derive(Debug, PartialEq, Eq)]
pub enum Command {
    Inc,
    Dec,
    Left,
    Right,
    Input,
    Output,
    OutputAscii,
    /// Incomplete jump pair, for internal use only.
    UnmatchedJumpForward,
    JumpForwardIfZero {
	addr: usize,
    },
    JumpBack {
	addr: usize,
    },
    /// Whitespace, for internal use only.
    Space,
}

#[derive(Debug, PartialEq, Eq)]
enum ErrorVariety {
    UnmatchedOpeningBracket,
    UnmatchedClosingBracket,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Error {
    variety: ErrorVariety,
    /// Index in the source string of the error.
    addr: usize,
}

pub fn compile(src: &str) -> Result<Vec<Command>, Error> {
    // There can be at most 1 command per source character, not counting comments & space.
    let mut prog: Vec<Command> = Vec::with_capacity(src.len());
    // The stack of addresses for jump commands.
    let mut jumps: Vec<usize> = Vec::new();

    for (i, symbol) in src.chars().enumerate() {
	let cmd = match symbol {
	    '+' => Ok(Command::Inc),
	    '-' => Ok(Command::Dec),
	    '<' => Ok(Command::Left),
	    '>' => Ok(Command::Right),
	    ',' => Ok(Command::Input),
	    '@' => Ok(Command::Output),
	    '.' => Ok(Command::OutputAscii),
	    '[' => {
		// The address to jump to can be added to this
		// when the accompanying closing bracket is found.
		jumps.push(prog.len());
		Ok(Command::UnmatchedJumpForward)
	    }
	    ']' => {
		if let Some(addr) = jumps.pop() {
		    Ok(Command::JumpBack { addr })
		} else {
		    // Mis-matched closing bracket.
		    Err(Error {
			addr: i,
			variety: ErrorVariety::UnmatchedClosingBracket,
		    })
		}
	    }
	    // Simply ignore other symbols: This allows in-line comments with no extra syntax.
	    _ => Ok(Command::Space),
	}?;
	match cmd {
	    Command::JumpBack { addr } => {
		if prog[addr] == (Command::UnmatchedJumpForward) {
		    // Jump to instruction after closing bracket.
		    prog[addr] = Command::JumpForwardIfZero {
			addr: prog.len() + 1,
		    };
		}
		prog.push(cmd);
	    }

	    // Skip spaces.
	    Command::Space => (),

	    _ => prog.push(cmd),
	}
    }

    if let Some(addr) = jumps.pop() {
	// Verify that all opening brackets were balanced.
	Err(Error {
	    addr,
	    variety: ErrorVariety::UnmatchedOpeningBracket,
	})
    } else {
	prog.shrink_to_fit();
	Ok(prog)
    }
}

#[cfg(test)]
mod compile_tests {
    use super::*;

    fn confirm_commands(src: &str, correct: Vec<Command>) {
	let prog = compile(src);
	match prog {
	    Ok(prog) => {
		for (a, b) in prog.iter().zip(correct.iter()) {
		    assert_eq!(a, b);
		}
		assert_eq!(prog.len(), correct.len());
	    }
	    _ => panic!("Program did not compile successfully: {}", src),
	}
    }

    #[test]
    fn linear() {
	confirm_commands(
	    "+-<>[],.",
	    vec![
		Command::Inc,
		Command::Dec,
		Command::Left,
		Command::Right,
		Command::JumpForwardIfZero { addr: 6 },
		Command::JumpBack { addr: 4 },
		Command::Input,
		Command::OutputAscii,
	    ],
	);
    }

    #[test]
    fn unmatched_open() {
	assert_eq!(
	    compile("["),
	    Err(Error {
		variety: ErrorVariety::UnmatchedOpeningBracket,
		addr: 0
	    })
	)
    }

    #[test]
    fn unmatched_close() {
	assert_eq!(
	    compile("]"),
	    Err(Error {
		variety: ErrorVariety::UnmatchedClosingBracket,
		addr: 0
	    })
	)
    }
}
