use std::env;
use std::fs;

pub mod interpretation;
pub mod parsing;

fn read_stdin() -> String {
    let mut code = String::new();
    while let Ok(bytes) = std::io::stdin().read_line(&mut code) {
        if bytes == 0 {
            break;
        }
    }
    code
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let mut compile: bool = false;
    let mut src = String::new();
    for arg in &args.as_slice()[1..] {
	match arg.as_str() {
	    "-" | "-i" => src.push_str(read_stdin().as_str()),
	    "-c" => compile = true,
	    default => src.push_str(
		fs::read_to_string(default)
		    .expect("Couldn't read file")
		    .as_str(),
	    ),
	}
    }

    println!("We are{} compiling", if compile { "" } else { " not" });
    match parsing::compile(src.as_str()) {
        Err(e) => panic!("{:?}", e),
        Ok(prog) => interpretation::execute(prog),
    };
}
